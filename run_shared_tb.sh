#!/usr/bin/env bash

iverilog -o ../dumpfiles/$1.compiled $1.v -Wall
vvp ../dumpfiles/$1.compiled -lxt2
gtkwave ../dumpfiles/$1.lxt $1.sav
