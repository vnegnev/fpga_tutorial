//-----------------------------------------------------------------------------
// Title         : counter
// Project       : fpga_tutorial
//-----------------------------------------------------------------------------
// File          : counter.v
// Author        :   <vlad@tiqi.localdomain>
// Created       : 09.09.2016
// Last modified : 09.09.2016
//-----------------------------------------------------------------------------
// Description :
// Basic counter
//-----------------------------------------------------------------------------
// Copyright (c) 2016 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 09.09.2016 : created
//-----------------------------------------------------------------------------

`ifndef _COUNTER_
`define _COUNTER_
`timescale 1ns/1ns

module counter(
	       input clk,
	       output reg[3:0] count_o
	       );

   initial begin
      count_o = 4'd0;
   end

   always @(posedge clk) begin
      count_o <= count_o + 4'd1;
   end
endmodule // counter

`endif //  `ifndef _COUNTER_
