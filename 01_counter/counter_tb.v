//-----------------------------------------------------------------------------
// Title         : counter_tb
// Project       : fpga_tutorial
//-----------------------------------------------------------------------------
// File          : counter_tb.v
// Author        :   <vlad@tiqi.localdomain>
// Created       : 09.09.2016
// Last modified : 09.09.2016
//-----------------------------------------------------------------------------
// Description :
// Testbench for counter
//-----------------------------------------------------------------------------
// Copyright (c) 2016 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 09.09.2016 : created
//-----------------------------------------------------------------------------

`ifndef _COUNTER_
 `include "counter.v"
`endif

`timescale 1ns/1ns

module counter_tb;
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg			clk;			// To UUT of counter.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [3:0]		count_o;		// From UUT of counter.v
   // End of automatics

   counter UUT(/*AUTOINST*/
	       // Outputs
	       .count_o			(count_o[3:0]),
	       // Inputs
	       .clk			(clk));

   always #5 clk = !clk;

   initial begin
      $dumpfile("../dumpfiles/counter_tb.lxt");
      $dumpvars(0, counter_tb);
      clk = 1;

      #350 $finish;
   end   

endmodule // counter_tb

   
