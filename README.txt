To run the examples you must be running a Unix-like environment able to run shell scripts, and have Icarus Verilog and GTKwave installed and accessible from the command line.

Each folder contains a simple Verilog example with a testbench. Run each using the run_tb.sh shell script.

Vlad
