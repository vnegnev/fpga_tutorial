//-----------------------------------------------------------------------------
// Title         : series_counter
// Project       : fpga_tutorial
//-----------------------------------------------------------------------------
// File          : series_counter.v
// Author        :   <vlad@tiqi.localdomain>
// Created       : 09.09.2016
// Last modified : 09.09.2016
//-----------------------------------------------------------------------------
// Description :
// Basic series_counter
//-----------------------------------------------------------------------------
// Copyright (c) 2016 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 09.09.2016 : created
//-----------------------------------------------------------------------------

`ifndef _SERIES_COUNTER_
`define _SERIES_COUNTER_
`timescale 1ns/1ns

module series_counter(
	       input clk,
	       output reg[3:0] count1_o
	       );

   reg [3:0] 		       count0;
   
   initial begin
      count0 = 4'd0;
      count1_o = 4'd0;
   end

   always @(posedge clk) begin
      count0 <= count0 + 4'd1;
      count1_o <= count1_o + count0;
   end
endmodule // series_counter

`endif //  `ifndef _SERIES_COUNTER_
