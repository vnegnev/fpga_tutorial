//-----------------------------------------------------------------------------
// Title         : simple_ram
// Project       : fpga_tutorial
//-----------------------------------------------------------------------------
// File          : simple_ram.v
// Author        :   <vlad@tiqi.localdomain>
// Created       : 11.09.2016
// Last modified : 11.09.2016
//-----------------------------------------------------------------------------
// Description :
// Simple single-port R/W RAM block
//-----------------------------------------------------------------------------
// Copyright (c) 2016 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 11.09.2016 : created
//-----------------------------------------------------------------------------

`ifndef _SIMPLE_RAM_
`define _SIMPLE_RAM_

`timescale 1ns/1ns

module simple_ram(
		  input clk,
		  input [1:0] addr_i,
		  input we_i,
		  input [3:0] data_i,
		  output reg [3:0] data_o
		  );
   reg [3:0] 		       ram[3:0];
   reg [3:0] 		       data_r;

   initial begin
      ram[0] = 4'd3;
      ram[1] = 4'd9;
      ram[2] = 4'd4;
      ram[3] = 4'd7;

      data_r = 4'd0;
      data_o = 4'd0;
   end

   always @(posedge clk) begin
      data_r <= ram[addr_i]; // 1-cycle delay
      data_o <= data_r;
      if (we_i) begin
	 ram[addr_i] <= data_i;
      end
   end   

endmodule // simple_ram
`endif //  `ifndef _SIMPLE_RAM_
