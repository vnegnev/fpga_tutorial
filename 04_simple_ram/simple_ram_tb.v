//-----------------------------------------------------------------------------
// Title         : simple_ram_tb
// Project       : fpga_tutorial
//-----------------------------------------------------------------------------
// File          : simple_ram_tb.v
// Author        :   <vlad@tiqi.localdomain>
// Created       : 11.09.2016
// Last modified : 11.09.2016
//-----------------------------------------------------------------------------
// Description :
// Testbench for simple_ram
//-----------------------------------------------------------------------------
// Copyright (c) 2016 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 11.09.2016 : created
//-----------------------------------------------------------------------------

`ifndef _SIMPLE_RAM_
 `include "simple_ram.v"
`endif

`timescale 1ns/1ns

module simple_ram_tb;
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg [1:0]		addr_i;			// To UUT of simple_ram.v
   reg			clk;			// To UUT of simple_ram.v
   reg [3:0]		data_i;			// To UUT of simple_ram.v
   reg			we_i;			// To UUT of simple_ram.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [3:0]		data_o;			// From UUT of simple_ram.v
   // End of automatics

   simple_ram UUT(/*AUTOINST*/
		  // Outputs
		  .data_o		(data_o[3:0]),
		  // Inputs
		  .clk			(clk),
		  .addr_i		(addr_i[1:0]),
		  .we_i			(we_i),
		  .data_i		(data_i[3:0]));

   always #5 clk = !clk;

   initial begin
      $dumpfile("../dumpfiles/simple_ram_tb.lxt");
      $dumpvars(0, simple_ram_tb);
      clk = 1;
      addr_i = 0;
      we_i = 0;
      data_i = 0;

      #6 addr_i = 1;
      #10 addr_i = 2;

      #30 addr_i = 0;
      data_i = 13;
      we_i = 1;
      
      #10 addr_i = 1;
      we_i = 0;

      #10 addr_i = 0;
      
      #10 addr_i = 2;
      data_i = 10;
      we_i = 1;
      
      #10 addr_i = 1;
      we_i = 0;
      #10 addr_i = 2;
      #10 addr_i = 0;

      #500 $finish;
   end   

endmodule // simple_ram_tb
