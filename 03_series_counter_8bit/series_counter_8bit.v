//-----------------------------------------------------------------------------
// Title         : series_counter_8bit
// Project       : fpga_tutorial
//-----------------------------------------------------------------------------
// File          : series_counter_8bit.v
// Author        :   <vlad@tiqi.localdomain>
// Created       : 09.09.2016
// Last modified : 09.09.2016
//-----------------------------------------------------------------------------
// Description :
// Basic series_counter
//-----------------------------------------------------------------------------
// Copyright (c) 2016 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 09.09.2016 : created
//-----------------------------------------------------------------------------

`ifndef _SERIES_COUNTER_8BIT_
`define _SERIES_COUNTER_8BIT_
`timescale 1ns/1ns

module series_counter_8bit(
	       input clk,
	       output reg[7:0] count1_o
	       );

   reg [7:0] 		       count0;
   
   initial begin
      count0 = 8'd0;
      count1_o = 8'd0;
   end

   always @(posedge clk) begin
      count0 <= count0 + 8'd1;
      count1_o <= count1_o + count0;
   end
endmodule // series_counter_8bit

`endif //  `ifndef _SERIES_COUNTER_8BIT_
