//-----------------------------------------------------------------------------
// Title         : series_counter_8bit_tb
// Project       : fpga_tutorial
//-----------------------------------------------------------------------------
// File          : series_counter_8bit_tb.v
// Author        :   <vlad@tiqi.localdomain>
// Created       : 09.09.2016
// Last modified : 09.09.2016
//-----------------------------------------------------------------------------
// Description :
// Testbench for series_counter_8bit
//-----------------------------------------------------------------------------
// Copyright (c) 2016 by TIQI, ETH Zurich This model is the confidential and
// proprietary property of TIQI, ETH Zurich and the possession or use of this
// file requires a written license from TIQI, ETH Zurich.
//------------------------------------------------------------------------------
// Modification history :
// 09.09.2016 : created
//-----------------------------------------------------------------------------

`ifndef _SERIES_COUNTER_8BIT_
 `include "series_counter_8bit.v"
`endif

`timescale 1ns/1ns

module series_counter_8bit_tb;
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg			clk;			// To UUT of series_counter_8bit.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [7:0]		count1_o;		// From UUT of series_counter_8bit.v
   // End of automatics

   series_counter_8bit UUT(/*AUTOINST*/
			   // Outputs
			   .count1_o		(count1_o[7:0]),
			   // Inputs
			   .clk			(clk));

   always #5 clk = !clk;

   initial begin
      $dumpfile("../dumpfiles/series_counter_8bit_tb.lxt");
      $dumpvars(0, series_counter_8bit_tb);
      clk = 1;

      #500 $finish;
   end   

endmodule // series_counter_8bit_tb

   
